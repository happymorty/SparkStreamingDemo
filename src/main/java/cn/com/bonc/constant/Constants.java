package cn.com.bonc.constant;

import cn.com.bonc.source.Source;

/**
 * 常量接口
 * @author lyq
 *
 */
public interface Constants {


    /*
    * Zk相关常量
    *
    */

    String ZK_SERVERS = "zk.servers";

    /**
	 * Spark作业相关的常量
	 */

	String SPARK_APP_NAME = "spark.app.name";
	String SPARK_DURATIONS_SECONDS= "spark.durations.seconds";
	String SPARK_STREAMING_STOPGRACEFULLYONSHUTDOWN = "spark.streaming.stopGracefullyOnShutdown";
	String SPARK_STREAMING_BACKPRESSURE_ENABLED = "spark.streaming.backpressure.enabled";
	String SPARK_STREAMING_BACKPRESSURE_INITIALRATE = "spark.streaming.backpressure.initialRate";
	String SPARK_STREAMING_KAFKA_MAXRATEPERPARTITION = "spark.streaming.kafka.maxRatePerPartition";
	String SPARK_SINK_CHECKPOINT="spark.sink.checkpoint";

	/**
	 * Kafka作业相关的常量
	 */

	String KAFKA_BOOTSTRAP_SERVERS = "kafka.bootstrap.servers";
	String KAFKA_GROUP_ID = "kafka.group.id";
	String KAFKA_MAX_POLL_INTERVAL_MS = "kafka.max.poll.interval.ms";
	String KAFKA_MAX_POLL_RECORDS = "kafka.max.poll.records";
	String KAFKA_FETCH_MAX_BYTES = "kafka.fetch.max.bytes";
	String KAFKA_MAX_PARTITION_FETCH_BYTES = "kafka.max.partition.fetch.bytes";
	String KAFKA_AUTO_OFFSET_RESET = "kafka.auto.offset.reset";
	String KAFKA_ENABLE_AUTO_COMMIT = "kafka.enable.auto.commit";
	String KAFKA_TOPICS = "kafka.topics";
	String KAFAK_SPARK_SINK_TOPICS="kafka.spark.sink.topics";


	/**
	 * HBase相关
	 */
	String CF_DEFAULT = "f";
	String HBASE_POOL_MAX_TOTAL = "hbase.pool.max-total";
	String HBASE_POOL_MAX_IDLE = "hbase.pool.max-idle";
	String HBASE_POOL_MAX_WAITMILLIS = "hbase.pool.max-waitmillis";
	String HBASE_POOL_TESTONBORROW = "hbase.pool.testonborrow";
	String HBASE_TABLE_NAME = "hbase.table.name";
	String HBASE_ZK_ZNODE_PARENT = "hbase.zookeeper.znode.parent";
	String HBASE_ZOOKEEPER_QUNRUM="hbase.zookeeper.quorum";


	String SEPARATOR_VERTICAL = "|";
	String RULE_ZK_NODE_PATH = "rule.zk.node.path";

	/**
	 * Redis相关
	 */
	String REDIS_MAX_TOTAL = "redis.max.total";
	String REDIS_MAX_IDLE = "redis.max.idle";
	String REDIS_MIN_IDLE = "redis.min.idle";
	String REDIS_TEST_BORROW = "redis.test.borrow";
	String REDIS_TEST_RETURN = "redis.test.return";
	String REDIS_IP = "redis.ip";
	String REDIS_PORT = "redis.port";

	/**
	 * window相关
	 */
	String WINDOW_DURATION = "windowSize";
	String SLIDE_DURATION = "slideSize";

	/**
	 * hdfs相关
	 */
	String HDFS_SOURCE_PATH = "hdfs.source.path";
	String HDFS_STATIC_PATH="hdfs.static.path";
	String HDFS_SAVE_PATH = "hdfs.save.path";

	/**
	 * 数据分割符号
	 */
	String DEFAULT_DATA_SEPARATOR="data.separator";

	String DEFAULT_SEPARATOR ="\\|";

	/**
	 * 数据源相关
	 */
	String HDFS_SOURCE="HDFS";
	String KAFKA_SOURCE="KAFKA";
	String ASSOCIATED_KAFKA_SOURCE ="A_KAFKA";
	String ASSOCIATED_REDIS_SOURCE ="A_REDIS";
	String ASSOCIATED_HDFS_SOURCE="A_HDFS";

}
