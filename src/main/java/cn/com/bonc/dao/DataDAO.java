/**
 * FileName: DAO
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/1/2 15:30
 * Description:
 */
package cn.com.bonc.dao;

public interface DataDAO<T1,T2> {

    void updateBatch(T1 t1) throws Exception;
    T1 query(T2 t2) throws Exception;

}
