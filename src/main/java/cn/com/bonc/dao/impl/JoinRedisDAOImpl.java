package cn.com.bonc.dao.impl;

import cn.com.bonc.dao.DataDAO;
//import cn.com.bonc.domain.Result;
//import cn.com.bonc.jdbc.RedisHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JoinRedisDAOImpl //implements DataDAO<List<Result>,List<Result>>
{

//    RedisHelper redisHelper = new RedisHelper();
//    @Override
//    public void updateBatch(List<Result> list) throws Exception {
//        //TODO
//        redisHelper.executeUpdate(list);
//    }
//
//    @Override
//    public List<Result> query(List<Result> list) throws Exception {
//
////        HashMultimap<String,String> keyFieldMap = HashMultimap.create();
//        //线程安全，使用parallelStream()并行时，需要考虑线程安全问题
////        SetMultimap<String,String> keyFieldMap = Multimaps.synchronizedSetMultimap(HashMultimap.create());
//        List<String> keyList = Collections.synchronizedList(new ArrayList<String>());
//        final List<Result> resultList = new ArrayList<Result>();
//        //先将所有的结果遍历出来，组装成key的List
//        list.parallelStream().forEach(r->{
//            keyList.add(r.getUserNumber());
//        });
//
//        //将keyList送给redis去查询
//        redisHelper.executeQuery(keyList,new RedisHelper.QueryCallback(){
//            /**
//             * 回调方法，rmap为结果集，即从Redis里查询出来的value
//             * 将事实数据，与rmap这个用户群的码表进行关联，如果大key和小key都存在，说明用户匹配。
//             * @param rmap
//             * @throws Exception
//             * @note resultList是个空的ArrayList，此处用addAll与关联后的结果集进行整合，形成最终结果集。
//             * 此处resultList不能重新赋值，只能addAll，而且，return的时候一定要return addAll语句中前面的list
//             * 例如，A.addAll(B) 那么，返回的时候要返回 return A，这样才是返回的A+B的结果，return B就只返回B
//             */
//            @Override
//            public void process(Map<String,Map<String,String>> rmap) throws Exception {
//                resultList.addAll(list.parallelStream().filter(result -> {
//                    boolean flag = false;
//                    Map<String,String> fieldMap = rmap.get(result.getUserNumber());
//                    if(fieldMap!=null){
//                        if(fieldMap.get(result.getBusinessType())!=null){
//                            flag = true;
//                        }
//                    }
//                    return flag;
//                }).collect(Collectors.toList()));
//            }
//        });
//        return resultList;
//    }
}
