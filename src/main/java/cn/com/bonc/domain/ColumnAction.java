/**
 * FileName: ColumnAction
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/1/21 10:13
 * Description:数据处理操作行为
 */
package cn.com.bonc.domain;

import java.util.ArrayList;

public class ColumnAction {
    /**与规则ID相对应的行为ID标识*/
    private String sceneID;

    /**配置的每个列对应的行为操作*/
    private ArrayList<ColumnCell> act;

    public String getSceneID() {
        return sceneID;
    }

    public void setSceneID(String sceneID) {
        this.sceneID = sceneID;
    }

    public ArrayList<ColumnCell> getAct() {
        return act;
    }

    public void setAct(ArrayList<ColumnCell> act) {
        this.act = act;
    }

    @Override
    public String toString() {
        return "ColumnAction{" +
                "sceneID='" + sceneID + '\'' +
                ", act=" + act +
                '}';
    }
}
