package cn.com.bonc.domain;

import java.io.Serializable;

public class ColumnCell implements Serializable {
    private int columnIndex;
    private String value;
    private String operator;

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return "ColumnCell{" +
                "columnIndex=" + columnIndex +
                ", value='" + value + '\'' +
                ", operator='" + operator + '\'' +
                '}';
    }
}