/**
 * FileName: ColumnAction
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/1/21 10:13
 * Description:数据筛选过滤以及行为
 * 改动：加入数据行为属性，以合并rule action ----> rule-action
 */
package cn.com.bonc.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class ColumnRule implements Serializable {

    /**规则的ID标识*/
    private String sceneID;

    /**一行中多列规则集合*/
    private ArrayList<ColumnCell> rules;

    /**配置的每个列对应的行为操作*/
    private ArrayList<ColumnCell> act;

    /**当前行规则对应的逻辑表达式*/
    private String logicalExpr;

    /**匹配后是否继续匹配下一条规则进行处理，默认为true，表示有下一条规则继续处理*/
    private boolean hasNextRule=true;

    /**连接外部数据进行筛选，状态：未开始*/
    private boolean joinUserGroup;

    public String getSceneID() {
        return sceneID;
    }

    public void setSceneID(String sceneID) {
        this.sceneID = sceneID;
    }

    public ArrayList<ColumnCell> getRules() {
        return rules;
    }

    public void setRules(ArrayList<ColumnCell> rules) {
        this.rules = rules;
    }

    public String getLogicalExpr() {
        return logicalExpr;
    }

    public void setLogicalExpr(String logicalExpr) {
        this.logicalExpr = logicalExpr;
    }

    public boolean isJoinUserGroup() {
        return joinUserGroup;
    }

    public void setJoinUserGroup(boolean joinUserGroup) {
        this.joinUserGroup = joinUserGroup;
    }

    public boolean isHasNextRule() {
        return hasNextRule;
    }

    public void setHasNextRule(boolean hasNextRule) {
        this.hasNextRule = hasNextRule;
    }

    public ArrayList<ColumnCell> getAct() {
        return act;
    }

    public void setAct(ArrayList<ColumnCell> act) {
        this.act = act;
    }

    @Override
    public String toString() {
        return "ColumnRule{" +
                "sceneID='" + sceneID + '\'' +
                ", rules=" + rules +
                ", act=" + act +
                ", logicalExpr='" + logicalExpr + '\'' +
                ", hasNextRule=" + hasNextRule +
                ", joinUserGroup=" + joinUserGroup +
                '}';
    }
}
