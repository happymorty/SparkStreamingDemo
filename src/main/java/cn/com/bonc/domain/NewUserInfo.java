package cn.com.bonc.domain;

/**
 * Created by ${RQL} on 2019/1/8
 */
public class NewUserInfo {
    private String phone;//手机号,不包含字冠
    private String LAC;//LAC参见“7.1省份区域编码”
    private String CL;//当有网络切换时，选择第一个CI
    private String IMEI;//终端类型
    private String Flow_Type;//流量类型
    private String StartingTime;//起始时间 精确到0.1微秒
    private String endingTime;//结束时间 精确到0.1微秒
    private String The_Last_Time;//持续时长
    private String The_Upward_Flow;//上行流量
    private String Downward_Flow;//下行流量
    private String traffic;//总流量
    private String Network_type;//网络类型
    private String The_Terminal_IP;//终端IP
    private String Access_IP;//访问IP
    private String Status_code;//状态码
    private String User_Agent;// 采集全部信息
    private String APN;//网段
    private String IMSI;//
    private String SGSN_IP;
    private String GGSN_IP;
    private String CONTENT_Type;//内容类型
    private String Source_Port;//源端口
    private String Destination_Port;//目的端口
    private String Record_Identifier;//记录标识 0 1 2 3
    private String Combined_Records;//合并记录数
    private String Url;//网址26位

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStartingTime() {
        return StartingTime;
    }

    public void setStartingTime(String startingTime) {
        StartingTime = startingTime;
    }

    public String getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(String endingTime) {
        this.endingTime = endingTime;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getLAC() {
        return LAC;
    }

    public void setLAC(String LAC) {
        this.LAC = LAC;
    }

    public String getCL() {
        return CL;
    }

    public void setCL(String CL) {
        this.CL = CL;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getFlow_Type() {
        return Flow_Type;
    }

    public void setFlow_Type(String flow_Type) {
        Flow_Type = flow_Type;
    }

    public String getThe_Last_Time() {
        return The_Last_Time;
    }

    public void setThe_Last_Time(String the_Last_Time) {
        The_Last_Time = the_Last_Time;
    }

    public String getThe_Upward_Flow() {
        return The_Upward_Flow;
    }

    public void setThe_Upward_Flow(String the_Upward_Flow) {
        The_Upward_Flow = the_Upward_Flow;
    }

    public String getDownward_Flow() {
        return Downward_Flow;
    }

    public void setDownward_Flow(String downward_Flow) {
        Downward_Flow = downward_Flow;
    }

    public String getTraffic() {
        return traffic;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }

    public String getNetwork_type() {
        return Network_type;
    }

    public void setNetwork_type(String network_type) {
        Network_type = network_type;
    }

    public String getThe_Terminal_IP() {
        return The_Terminal_IP;
    }

    public void setThe_Terminal_IP(String the_Terminal_IP) {
        The_Terminal_IP = the_Terminal_IP;
    }

    public String getAccess_IP() {
        return Access_IP;
    }

    public void setAccess_IP(String access_IP) {
        Access_IP = access_IP;
    }

    public String getStatus_code() {
        return Status_code;
    }

    public void setStatus_code(String status_code) {
        Status_code = status_code;
    }

    public String getUser_Agent() {
        return User_Agent;
    }

    public void setUser_Agent(String user_Agent) {
        User_Agent = user_Agent;
    }

    public String getAPN() {
        return APN;
    }

    public void setAPN(String APN) {
        this.APN = APN;
    }

    public String getIMSI() {
        return IMSI;
    }

    public void setIMSI(String IMSI) {
        this.IMSI = IMSI;
    }

    public String getSGSN_IP() {
        return SGSN_IP;
    }

    public void setSGSN_IP(String SGSN_IP) {
        this.SGSN_IP = SGSN_IP;
    }

    public String getGGSN_IP() {
        return GGSN_IP;
    }

    public void setGGSN_IP(String GGSN_IP) {
        this.GGSN_IP = GGSN_IP;
    }

    public String getCONTENT_Type() {
        return CONTENT_Type;
    }

    public void setCONTENT_Type(String CONTENT_Type) {
        this.CONTENT_Type = CONTENT_Type;
    }

    public String getSource_Port() {
        return Source_Port;
    }

    public void setSource_Port(String source_Port) {
        Source_Port = source_Port;
    }

    public String getDestination_Port() {
        return Destination_Port;
    }

    public void setDestination_Port(String destination_Port) {
        Destination_Port = destination_Port;
    }

    public String getRecord_Identifier() {
        return Record_Identifier;
    }

    public void setRecord_Identifier(String record_Identifier) {
        Record_Identifier = record_Identifier;
    }

    public String getCombined_Records() {
        return Combined_Records;
    }

    public void setCombined_Records(String combined_Records) {
        Combined_Records = combined_Records;
    }

}
