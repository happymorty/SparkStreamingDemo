/**
 * FileName: Xdata
 * Author:   SAMSUNG-PC ???
 * Date:     2019/2/25 14:30
 * Description:?????Redis??????????
 */
package cn.com.bonc.domain;

public class Xdata {

    private String key;

    private String value;

    public Xdata(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Xdata{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
