/**
 * FileName: OrderedProperties
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2018/12/20 14:40
 * Description:保证读取出的Properties是有序的
 */
package cn.com.bonc.dynamic;

import java.util.*;

public class OrderedProperties extends Properties {

    private static final long serialVersionUID = -208089746760912462L;

    private final LinkedHashSet<Object> keys = new LinkedHashSet<Object>();
 
    public Enumeration<Object> keys() {
        return Collections.<Object> enumeration(keys);
    }
 
    public Object put(Object key, Object value) {
        keys.add(key);
        return super.put(key, value);
    }
 
    public Set<Object> keySet() {
        return keys;
    }
 
    public Set<String> stringPropertyNames() {
        Set<String> set = new LinkedHashSet<String>();
 
        for (Object key : this.keys) {
            set.add((String) key);
        }
 
        return set;
    }
}