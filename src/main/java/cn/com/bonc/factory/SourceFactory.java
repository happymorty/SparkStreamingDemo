/**
 * FileName: SourceFactory
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 17:54
 * Description:
 */
package cn.com.bonc.factory;

import cn.com.bonc.constant.Constants;
import cn.com.bonc.source.Source;
import cn.com.bonc.source.impl.*;

public class SourceFactory {

    public static Source create(String source){
        switch (source){
            case Constants.ASSOCIATED_REDIS_SOURCE:
                return new AssociatedRedisSourceImpl();
            case Constants.ASSOCIATED_KAFKA_SOURCE:
                return new AssociatedHDFSSourceImpl();
            case Constants.KAFKA_SOURCE:
                return new KafkaSourceImpl();
            case Constants.HDFS_SOURCE:
                return new HDFSSourceImpl();
        }
        return new DefaultSourceImpl();
    }
}
