package cn.com.bonc.jdbc;

import cn.com.bonc.conf.ConfigurationManager;
import cn.com.bonc.constant.Constants;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RedisHelper {

    //jedis连接池
    private static JedisPool pool;
    //与redis连接池连接的最大连接数
    private static Integer maxTotal = ConfigurationManager.getInteger(Constants.REDIS_MAX_TOTAL);
    //在这个连接池中最多有多少个状态为idle的jedis实例，jedis连接池里就是jedis的实例，idle就是空闲的jedis实例
    //在jedis连接池中最大的idle状态（空闲的）的jedis实例的个数
    private static Integer maxIdle = ConfigurationManager.getInteger(Constants.REDIS_MAX_IDLE);
    //在jedis连接池中最小的idle状态（空闲的）的jedis实例的个数
    private static Integer minIdle = ConfigurationManager.getInteger(Constants.REDIS_MIN_IDLE);

    //在borrow一个jedis实例的时候，是否要进行验证操作，如果赋值为true，则得到的jedis实例肯定是可用的
    private static Boolean testOnBorrow = Boolean.parseBoolean(Constants.REDIS_TEST_BORROW);
    //在return一个jedis实例的时候，是否要进行验证操作，如果赋值为true，则返回jedis连接池的jedis实例肯定是可用的
    private static Boolean testOnReturn = Boolean.parseBoolean(Constants.REDIS_TEST_RETURN);

    private static String redisIp = ConfigurationManager.getProperty(Constants.REDIS_IP);
    private static Integer redisPort = ConfigurationManager.getInteger(Constants.REDIS_PORT);

    static {
        initPool();
    }

    //初始化连接池，只会调用一次
    private static void initPool() {
        JedisPoolConfig config = new JedisPoolConfig();

        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);

        config.setTestOnBorrow(testOnBorrow);
        config.setTestOnReturn(testOnReturn);

        //连接池耗尽的时候，是否阻塞，false会抛出异常，true阻塞直到超时，会抛出超时异常，默认为true
        config.setBlockWhenExhausted(true);

        //这里超时时间是2s
        pool = new JedisPool(config, redisIp, redisPort, 1000*2);

    }

	/**
	 * 执行查询SQL语句，将数据
     *
	 * @param callback
	 */
	@SuppressWarnings("static-access")
	public void executeQuery(List<String> keyList,
			QueryCallback callback) throws Exception {

        // 1.生成pipeline对象
        Jedis jedis = pool.getResource();

	}

    /**
     * 执行插入操作，用于将本地数据上传至redis
     * 未实现方法，需要根据情况自行决定，传入参数类型及个数
     *
     */
    @SuppressWarnings("static-access")
    public void executeUpdate() throws Exception {

        // 1.生成pipeline对象
        Jedis jedis = pool.getResource();
        Pipeline pipeline = jedis.pipelined();
    }


	/**
	 * 静态内部类：查询回调接口
	 * @author Administrator
	 *
	 */
	public static interface QueryCallback {
		
		/**
		 * 处理查询结果
		 * @param rmap
		 * @throws Exception
		 */
		void process(Map<String, Map<String, String>> rmap) throws Exception;
		
	}
	
}
