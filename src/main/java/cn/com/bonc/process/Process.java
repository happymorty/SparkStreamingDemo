/**
 * FileName: Process
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/1/17 10:42
 * Description:
 */
package cn.com.bonc.process;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public interface Process {
    Dataset<Row> processing(Dataset<Row> rowDataset);
}
