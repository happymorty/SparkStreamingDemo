/**
 * FileName: JoinRedisProcessImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 10:57
 * Description:
 */
package cn.com.bonc.process.impl;

import cn.com.bonc.process.Process;
import cn.com.bonc.util.ProcessCfgUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class JoinRedisProcessImpl implements Process {


    private SparkSession sparkSession;

    public JoinRedisProcessImpl(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    @Override
    public Dataset<Row> processing(Dataset<Row> rowDataset) {

        if (ProcessCfgUtil.isJoin()){
            sparkSession.read()
                    .format("org.apache.spark.sql.redis")
                    .option("table", "Xdata")
                    .load()
                    .registerTempTable("redis");
        }
        return rowDataset;
    }
}
