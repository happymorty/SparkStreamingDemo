/**
 * FileName: SqlListProcessImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 11:08
 * Description:
 */
package cn.com.bonc.process.impl;

import cn.com.bonc.process.Process;
import cn.com.bonc.util.ProcessCfgUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SqlListProcessImpl implements Process {


    private SparkSession sparkSession;

    public SqlListProcessImpl(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    @Override
    public Dataset<Row> processing(Dataset<Row> rowDataset) {
        for (String sqlStr : ProcessCfgUtil.getSqlList()) {
            rowDataset.createOrReplaceTempView("kafka");
            rowDataset = sparkSession.sql(sqlStr);
        }
        return rowDataset;
    }
}
