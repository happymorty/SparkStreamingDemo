/**
 * FileName: WindowCountProcessImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 11:10
 * Description:
 */
package cn.com.bonc.process.impl;

import cn.com.bonc.process.Process;
import cn.com.bonc.util.ProcessCfgUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class WindowCountProcessImpl implements Process {
    @Override
    public Dataset<Row> processing(Dataset<Row> rowDataset) {
        boolean windowAggregate = ProcessCfgUtil.isWindowAggregate();
        if (windowAggregate){
            //特定窗口时长内手机号出现的次数
            rowDataset = rowDataset
                    .groupBy(ProcessCfgUtil.getWindowColsWithTimestamp())
                    .count();
        }
        return rowDataset;
    }
}
