/**
 * FileName: AssociatedHDFSSourceImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 19:27
 * Description:
 */
package cn.com.bonc.source.impl;

import cn.com.bonc.conf.ConfigurationManager;
import cn.com.bonc.constant.Constants;
import cn.com.bonc.source.Source;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class DefaultSourceImpl implements Source<Row>{
    @Override
    public Dataset<Row> getDataset(SparkSession sparkSession) {
        return null;
    }
}
