/**
 * FileName: ExternalResourceUtil
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/2/14 11:00
 * Description:加载spark动态外部资源文件
 * rule.json                    筛选规则文件
 * action.json                  与规则对应的行为文件
 * redis.txt                    Join redis 时需要的数据
 * column-mapping.properties    列转换配置文件
 * 
 * DataFilterAndOperatorUtil.class
 *      .getClassLoader()
 *      .getResourceAsStream(XXX_RESOURCE_PATH);
 */
package cn.com.bonc.util;

import cn.com.bonc.conf.ConfigurationManager;
import cn.com.bonc.dao.DataDAO;
import cn.com.bonc.domain.ColumnAction;
import cn.com.bonc.domain.ColumnCell;
import cn.com.bonc.domain.ColumnRule;
import cn.com.bonc.domain.Xdata;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.*;

public class ExternalResourceUtil {

    /**规则文件*/
    private static final String RULE_RESOURCE_PATH = "rule.json";

    /**与规则文件相匹配的数据操作文件*/
    private static final String ACTION_RESOURCE_PATH = "action.json";

    /**规则与的数据操作配置文件*/
    private static final String RULE_ACTION_RESOURCE_PATH = "rule-action.json";

    /**列分割配置文件，将单列分割为多列*/
    private static final String COLUM_MAPPING_RESOURCE_PATH = "column-mapping.properties";

    /**Join redis 时需要的数据*/
    private static final String REDIS_RESOURCE_PATH="redis.txt";
    

    /**
     *
     * @throws IOException
     */
    public static List<ColumnRule> loadRuleJsonFileData() throws IOException {
        String content = IOUtils.toString(new FileInputStream(RULE_RESOURCE_PATH));
        List<ColumnRule> rulesList = JSON.parseArray(content,ColumnRule.class);
        return rulesList;

    }

    /**
     * 加载对列数据
     * @throws IOException
     */
    public static HashMap<String, List<ColumnCell>> loadActionJsonFileData() throws IOException {
        String content = IOUtils.toString(new FileInputStream(ACTION_RESOURCE_PATH));
        List<ColumnAction> columnActionList = JSON.parseArray(content,ColumnAction.class);
        HashMap<String, List<ColumnCell>> actionMap = new HashMap<>();
        columnActionList.stream().forEach(x-> actionMap.put(x.getSceneID(),x.getAct()));
        return actionMap;
    }

    /**
     *  加载对列筛选,操作的文件
     * @return
     * @throws IOException
     */
    public static List<ColumnRule> loadRuleActionsJsonFileData() throws IOException {
        String content = IOUtils.toString(new FileInputStream(RULE_ACTION_RESOURCE_PATH));
        List<ColumnRule> rulesList = JSON.parseArray(content,ColumnRule.class);
        return rulesList;
    }


    /**
     *  加载被Join的Redis原始数据
     *  每条数据格式   xxx,xxx
     * @return List<Xdata> 集合数据
     * @throws IOException 出现IO异常时，抛出异常
     */
    public static List<Xdata> loadRedisTxtFileData() throws IOException {
        List<Xdata> xdataList =new ArrayList<>();
        String content = IOUtils.toString(new FileInputStream(REDIS_RESOURCE_PATH));
        String[] split = StringUtils.split(content,"\n");
        System.out.println(split.length);
        for (String s:split) {
            String[] line = s.split(",");
            if (line.length==2){
                xdataList.add(new Xdata(line[0],line[1]));
            }
        }
        return xdataList;
    }

    /**
     * 从特定的位置加载上传的redis数据
     * @param path 数据位置
     * @return List<Xdata> 集合数据
     * @throws IOException 出现IO异常时，抛出异常
     */
    public static List<Xdata> loadRedisTxtFileData(String path) throws IOException {
        FileReader fr = null;
        BufferedReader br = null;
        List<Xdata> xdataList =new ArrayList<>();
        for (File file : new File(path).listFiles()) {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String line = null;
            while ((line = br.readLine()) != null) {
                String[] lineArray = line.split(",");
                if (lineArray.length==2){
                    xdataList.add(new Xdata(lineArray[0],lineArray[1]));
                }
            }
        }
        return xdataList;
    }

    /**
     * 加载单列映射为多列所需的Properties配置文件
     * @return properties
     * @throws IOException 
     */
    public static Properties loadColumnMappingPropertiesFileData() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(COLUM_MAPPING_RESOURCE_PATH));
        return properties;
    }

}
