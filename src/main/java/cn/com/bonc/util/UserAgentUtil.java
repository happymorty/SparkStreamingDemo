package cn.com.bonc.util;

import cz.mallat.uasparser.OnlineUpdater;
import cz.mallat.uasparser.UASparser;
import cz.mallat.uasparser.UserAgentInfo;

import java.io.IOException;

/**
 * Created by ${RQL} on 2019/1/15
 */
public class UserAgentUtil {



         static UASparser uasParser = null;

         // 初始化uasParser对象
         static {
             try {
                 uasParser = new UASparser(OnlineUpdater.getVendoredInputStream());
                 } catch (IOException e) {
                 e.printStackTrace();
                 }
             }

         public static void main(String[] args)
 {
            //String str = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";
             String str = "Dalvik/2.1.0 (Linux; U; Android 8.1.0; SM-N9600 Build/M1AJQ)";
             System.out.println(str);
             try {
                 UserAgentInfo userAgentInfo = UserAgentUtil.uasParser.parse(str);
                 System.out.println("操作系统名称:"+userAgentInfo.getOsFamily());//
                 System.out.println("操作系统:"+userAgentInfo.getOsName());//
                 System.out.println("浏览器名称:"+userAgentInfo.getUaFamily());//
                 System.out.println("浏览器版本:"+userAgentInfo.getBrowserVersionInfo());//
                 System.out.println("设备类型:"+userAgentInfo.getDeviceType());
                 System.out.println("浏览器:"+userAgentInfo.getUaName());
                 System.out.println("类型:"+userAgentInfo.getType());
                 System.out.println("图标:"+userAgentInfo.getDeviceIcon());
                 System.out.println("1:"+userAgentInfo.getDeviceType());
                 System.out.println("操作系统公司:"+userAgentInfo.getOsCompany());
                 System.out.println("操作系统图标:"+userAgentInfo.getOsIcon());

                 } catch (IOException e) {
                 e.printStackTrace();
                 }
             }

}
